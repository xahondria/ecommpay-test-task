import $ from 'jquery';
import utils from './js/utils/utils';
import cardsData from './js/mock/mock-data';
import Card from './js/components/card';

const cardsToRender = utils.splitArrayByGroups(cardsData, 4);
const container = $('.news__card-list');

let groupIndex = 0;
$('.news__button').click((ev) => {
  ev.preventDefault();
  if (groupIndex >= cardsToRender.length) {
    $('.news__button').removeClass('btn-outline-quaternary').addClass('btn-outline-success').text('NO MORE NEWS!');
    return;
  }
  let fragment = document.createDocumentFragment();
  cardsToRender[groupIndex].forEach((cardData) => {
    const card = new Card(cardData);
    card.renderTo(fragment);
  });
  $(container).append(fragment);
  groupIndex++;
});
