const utils = {
  splitArrayByGroups(array, groupLength = 4) {
    let groupIndex = 0;
    return array.reduce((memo, current) => {
      if (memo[groupIndex].length >= groupLength) {
        groupIndex++;
        memo.push([]);
      }
      memo[groupIndex].push(current);
      return memo;
    }, [[]]);
  },

  getRandomInt(max) {
    return Math.floor(Math.random() * (max + 1));
  },

};

export default utils;
