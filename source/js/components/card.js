import $ from 'jquery';

class Card {
  constructor(data) {
    this._props = {
      hashtag: data.hashtag,
      news: data.news,
    };

    this._element = null;
  }

  get template() {
    return `
      <div class="col-md-6 news__card-wrapper">
        <div class="news__card">
          <p class="news__hashtag">#${this._props.hashtag}</p>
          <p class="news__text">${this._props.news}</p>
          <a class="news__link" href="#">Read More</a>
        </div>
      </div>
    `.trim();
  }

  createElement() {
    this._element = $.parseHTML(this.template)[0];
  }

  renderTo(container) {
    this.createElement();
    $(container).append(this._element);
  }
}

export default Card;
