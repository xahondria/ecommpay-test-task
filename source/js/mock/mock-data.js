import utils from '../utils/utils';

const rawData = {
  hashtags: [
    'news',
    'casestudy',
    'whitepaper',
    'transfer',
    'safety',
    'eCommerce',
  ],
  news: [
    'The former chief executive of the carmaker Volkswagen has been charged in Germany over his involvement in the company\'s diesel emissions scandal.',
    'Drinks giant Diageo has announced that it is removing plastic from multipacks of its Irish stout brand Guinness.',
    'Last week, Mr Ma wrote that without the system, China\'s economy was "very likely to lose vitality and impetus".',
    'The country has enjoyed economic growth averaging 10% for more than 25 years - from the late 1970s to the mid 2000s - but in subsequent years that has slowed to nearer 6%.',
    'Mr Liu, who started the company that would become JD.com in 1998, recently wrote about his attitude to work, saying he used to set his alarm to wake him up every two hours to make sure he could offer his customers a full, 24-hour, service.',
    'He wrote: "JD in the last four, five years has not made any eliminations, so the number of staff has expanded rapidly, the number of people giving orders has grown and grown, while the those who are working have fallen.',
    'Mr Ma co-founded Alibaba, sometimes called China\'s eBay, in 1999 and has seen it become one of the world\'s biggest internet companies.',
  ],
};

function generateCardData() {
  return {
    get hashtag() {
      return rawData.hashtags[utils.getRandomInt(rawData.hashtags.length - 1)];
    },
    get news() {
      return rawData.news[utils.getRandomInt(rawData.news.length - 1)];
    },
  }
}

const cardsData = [...Array(11)].map(generateCardData);

export default cardsData;
